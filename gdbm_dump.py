#!/bin/env python
# -*- coding: utf-8 -*-

import sys
import bz2
import pickle
import gdbm

def dump(dbname, filename, is_bz2=False):
    if filename == "-":
        fp = sys.stdout
    elif is_bz2:
        fp = bz2.BZ2File(filename, "w")
    else:
        fp = open(filename, "w")

    db = gdbm.open(dbname)

    for k in db.keys():
        pickle.dump((k, db[k]), fp)

    db.close()

    if fp != sys.stdout:
        fp.close()

def main():
    argc = len(sys.argv) - 1
    argv = sys.argv[1:]

    if argc < 1 or (argc == 3 and argv[0] != "-j") or argc > 3:
        print 'Usage: gdbm_dump.py [-j] DB_FILE [FILE]'
        exit()

    is_bz2 = False
    if argv[0] == "-j":
        is_bz2 = True
        argc -= 1
        del argv[0]

    dbname = argv[0]
    if argc == 1:
        filename = "-"
    else:
        filename = argv[1]
    
    dump(dbname, filename, is_bz2)

    return 0

if __name__ == "__main__":
    sys.exit(main())
