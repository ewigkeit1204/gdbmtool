#!/bin/env python
# -*- coding: utf-8 -*-

import sys
import bz2
import argparse
import pickle
import gdbm

def load(filename, dbname, bzip2=False, replace=False, mode=None):
    if filename == "-":
        fp = sys.stdin
    elif bzip2:
        fp = bz2.BZ2File(filename, "r")
    else:
        fp = open(filename, "r")

    flag = "n" if replace else "c"

    if mode is not None:
        db = gdbm.open(dbname, flag, mode)
    else:
        db = gdbm.open(dbname, flag)

    while True:
        try:
            (k, v) = pickle.load(fp)
        except EOFError:
            break
        db[k] = v

    db.close()

    if fp != sys.stdin:
        fp.close()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-j", "--bzip2", default=False, help="filter the archive through bzip2")
    parser.add_argument("-r", "--replace", default=False, help="If the database exists, replace records in it.")
    parser.add_argument("-m", "--mode", type=int, help="Set database file mode (octal number)")
    parser.add_argument("FILE")
    parser.add_argument("DB_FILE")

    args = parser.parse_args()

    load(args.FILE, args.DB_FILE, args.bzip2, args.replace, args.mode)

    return 0

if __name__ == "__main__":
    sys.exit(main())

